# wallpaper-engine-unpkg

## 介绍

wallpaper engine 引擎解包工具

## Windows-X64版

如果系统中未安装jvm, 可直接下载此可执行文件

###下载地址 [unpack-0.0.1.exe](https://share.weiyun.com/5BarzVK)

###运行方式: 

双击打开

	unpack-0.0.1.exe

再根据提示运行

	unpack <pkg文件> [可选<输出目录>]
	
例子:

	unpack C:\MyProject\scene.pkg //此时文件会解包到C:\MyProject\目录下
	
	unpack C:\MyProject\scene.pkg D:\MyProjectTwo
	
## jar版本

###下载地址 [wallpaper-engine-unpkg.jar](https://share.weiyun.com/5pwOyoE)

###运行方式: 

	java -jar wallpaper-engine-unpkg.jar C:\MyProject\scene.pkg
	
	java -jar wallpaper-engine-unpkg.jar C:\MyProject\scene.pkg D:\MyProjectTwo
	
## 编译并运行

### Windows

仅编译jar文件 

	.\unpack.bat
	
编译并运行 

	.\unpack.bat C:\MyProject\scene.pkg
	
### *nix

仅编译jar文件 
	
	sh ./unpack.sh
	
编译并运行 

	sh ./unpack.sh C:\MyProject\scene.pkg


## 参与贡献


+ Fork 本仓库
+ 新建 Feat_xxx 分支
+ 提交代码
+ 新建 Pull Request

