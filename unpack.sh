#!/bin/sh
if [ ! -f "./wallpaper-engine-unpkg.jar" ]; then
	rm -r ./bin/
	find -name "*.java" > sources.list
	javac --release 7 -encoding UTF-8 @sources.list -d ./bin/ -verbose
    jar cvfm ./wallpaper-engine-unpkg.jar ./src/META-INF/MANIFEST.MF -C ./bin/ ./
fi
if [ X$1 = "X" ];then
	java -jar wallpaper-engine-unpkg.jar
else java -jar wallpaper-engine-unpkg.jar "$1" "$2"
fi

