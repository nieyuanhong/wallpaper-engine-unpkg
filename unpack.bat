@echo off
if not exist ./wallpaper-engine-unpkg.jar (
	del /q .\bin
	dir /s /B *.java > sources.list
	javac --release 7 -encoding UTF-8 @sources.list -d ./bin/ -verbose
	jar cvfm ./wallpaper-engine-unpkg.jar ./src/META-INF/MANIFEST.MF -C ./bin/ ./
)
java -jar wallpaper-engine-unpkg.jar %1 %2
pause